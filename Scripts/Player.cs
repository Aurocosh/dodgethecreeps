using Godot;
using System;

public class Player : Area2D
{
	[Signal]
	public delegate void Hit();

	[Export]
	public int Speed = 400;

	private Vector2 _screenSize;
	private Vector2 _target;
	private bool _targetIsSet;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_screenSize = GetViewport().Size;
		Hide();
	}

	public void Start(Vector2 pos)
	{
		Position = pos;
		_target = pos;
		_targetIsSet = false;
		Show();
		GetNode<CollisionShape2D>("CollisionShape2D").Disabled = false;
	}

	public override void _Input(InputEvent @event)
	{
		if (@event is InputEventScreenTouch eventScreenTouch && eventScreenTouch.Pressed)
		{
			_target = eventScreenTouch.Position;
			_targetIsSet = true;
		}
	}

	//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		var velocity = new Vector2();

		if (_targetIsSet)
		{
			if (Position.DistanceTo(_target) > 10)
				velocity = _target - Position;
			else
				_targetIsSet = false;
		}
		else
		{
			if (Input.IsActionPressed("ui_right"))
			{
				velocity.x += 1;
			}
			if (Input.IsActionPressed("ui_left"))
			{
				velocity.x -= 1;
			}
			if (Input.IsActionPressed("ui_down"))
			{
				velocity.y += 1;
			}
			if (Input.IsActionPressed("ui_up"))
			{
				velocity.y -= 1;
			}
		}

		var animatedSprite = GetNode<AnimatedSprite>("AnimatedSprite");

		if (velocity.Length() > 0)
		{
			animatedSprite.Play();
			velocity = velocity.Normalized() * Speed;
			Position += velocity * delta;
			Position = new Vector2(
				x: Mathf.Clamp(Position.x, 0, _screenSize.x),
				y: Mathf.Clamp(Position.y, 0, _screenSize.y)
			);
		}
		else
		{
			animatedSprite.Stop();
		}

		if (velocity.x != 0)
		{
			animatedSprite.Animation = "walk";
			animatedSprite.FlipV = false;
			animatedSprite.FlipH = velocity.x < 0;
		}
		else if (velocity.y != 0)
		{
			animatedSprite.Animation = "up";
			animatedSprite.FlipV = velocity.y > 0;
		}
	}

	private void _on_Player_body_entered(object body)
	{
		// Replace with function body.
		Hide();
		EmitSignal("Hit");
		GetNode<CollisionShape2D>("CollisionShape2D").SetDeferred("disabled", true);
	}
}
